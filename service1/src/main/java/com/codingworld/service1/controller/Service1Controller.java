package com.codingworld.service1.controller;

import com.codingworld.service1.service.WorksheetService;
import domain.WorksheetEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/rs")
public class Service1Controller {
  @Autowired
    private WorksheetService worksheetService;

  @GetMapping(name = "/worksheet",value = "/worksheet")
  public String sayHello() {

    return "Hello From Worksheet Service";
  }
  @PostMapping(name = "/worksheet",value = "/worksheet")
  public void createWorksheet(@Valid @RequestBody WorksheetEntity worksheet) {
    worksheetService.createWorksheet(worksheet);
    // create worksheet
  }



}
