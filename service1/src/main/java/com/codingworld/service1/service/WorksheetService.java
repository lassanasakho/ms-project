package com.codingworld.service1.service;

import domain.WorksheetEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WorksheetService {
    WorksheetEntity createWorksheet(WorksheetEntity worksheet);

    WorksheetEntity getWorksheetById(long l);

    List<WorksheetEntity> getAllWorksheets();

}
