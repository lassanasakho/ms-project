package com.codingworld.service1.service;

import com.codingworld.service1.repository.WorksheetRepository;
import domain.WorksheetEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorksheetServiceImpl implements WorksheetService{

    @Autowired
    private WorksheetRepository worksheetRepository;
    @Override
    public WorksheetEntity createWorksheet(WorksheetEntity worksheet) {
        return worksheetRepository.saveAndFlush(worksheet);
    }

    public WorksheetEntity getWorksheetById(long l) {
        return worksheetRepository.findById(l).get();
    }

    public List<WorksheetEntity> getAllWorksheets() {
        return worksheetRepository.findAll();
    }
}
