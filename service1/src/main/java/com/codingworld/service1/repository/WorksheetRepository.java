package com.codingworld.service1.repository;

import domain.WorksheetEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorksheetRepository extends JpaRepository<WorksheetEntity, Long> {

}
