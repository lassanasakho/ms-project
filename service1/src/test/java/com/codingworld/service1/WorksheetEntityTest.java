package com.codingworld.service1;

import domain.WorksheetEntity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorksheetEntityTest {

    @Test
    public void testGettersAndSetters() {
        WorksheetEntity worksheet = new WorksheetEntity();
        worksheet.setId(1L);
        worksheet.setName("Test Worksheet");
        worksheet.setProjectId(2L);

        assertEquals(1L, worksheet.getId());
        assertEquals("Test Worksheet", worksheet.getName());
        assertEquals(2L, worksheet.getProjectId());
    }
}
