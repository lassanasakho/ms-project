package com.codingworld.service1;

import com.codingworld.service1.repository.WorksheetRepository;
import com.codingworld.service1.service.WorksheetServiceImpl;
import domain.WorksheetEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WorksheetServiceTest {

    @Mock
    private WorksheetRepository worksheetRepository;

    @InjectMocks
    private WorksheetServiceImpl worksheetService;

    @Test
    void testCreateWorksheet() {
        // Arrange
        WorksheetEntity mockWorksheet = new WorksheetEntity();
        mockWorksheet.setId(1L);
        mockWorksheet.setName("Test Worksheet");
        when(worksheetRepository.saveAndFlush(any(WorksheetEntity.class))).thenReturn(mockWorksheet);
        // Act
        WorksheetEntity createdWorksheet = worksheetService.createWorksheet(new WorksheetEntity());
        // Assert
        assertEquals(mockWorksheet.getId(), createdWorksheet.getId());
        assertEquals(mockWorksheet.getName(), createdWorksheet.getName());
    }
    @Test
    void testGetWorksheetById() {
        // Arrange
        WorksheetEntity mockWorksheet = new WorksheetEntity();
        mockWorksheet.setId(1L);
        mockWorksheet.setName("Test Worksheet");
        when(worksheetRepository.findById(1L)).thenReturn(Optional.of(mockWorksheet));
        // Act
        WorksheetEntity retrievedWorksheet = worksheetService.getWorksheetById(1L);
        // Assert
        assertNotNull(retrievedWorksheet);
        assertEquals(mockWorksheet.getId(), retrievedWorksheet.getId());
        assertEquals(mockWorksheet.getName(), retrievedWorksheet.getName());
    }

    @Test
    void testGetAllWorksheets() {
        // Arrange
        WorksheetEntity worksheet1 = new WorksheetEntity();
        worksheet1.setId(1L);
        worksheet1.setName("Worksheet 1");
        WorksheetEntity worksheet2 = new WorksheetEntity();
        worksheet2.setId(2L);
        worksheet2.setName("Worksheet 2");
        when(worksheetRepository.findAll()).thenReturn(Arrays.asList(worksheet1, worksheet2));
        // Act
        List<WorksheetEntity> worksheets = worksheetService.getAllWorksheets();
        // Assert
        assertNotNull(worksheets);
        assertEquals(2, worksheets.size());
    }

    // Ajoutez d'autres tests selon vos besoins
}
