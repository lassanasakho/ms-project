package com.codingworld.service1;

import domain.ProjectEntity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProjectEntityTest  {

    @Test
    public void testGettersAndSetters() {
        ProjectEntity project = new ProjectEntity();
        project.setId(1L);
        project.setName("Test Project");
        assertEquals(1L, project.getId());
        assertEquals("Test Project", project.getName());
    }
    @Test
    public void testEquals() {
        ProjectEntity project1 = new ProjectEntity(1L, "Test Project");
        ProjectEntity project2 = new ProjectEntity(1L, "Test Project");
        ProjectEntity project3 = new ProjectEntity(2L, "Another Project");

        assertTrue(project1.equals(project2)); // Same id and name
        assertFalse(project1.equals(project3)); // Different id and name
        assertFalse(project1.equals(null)); // Not equal to null
    }

    @Test
    public void testHashCode() {
        ProjectEntity project1 = new ProjectEntity(1L, "Test Project");
        ProjectEntity project2 = new ProjectEntity(1L, "Test Project");

        assertEquals(project1.hashCode(), project2.hashCode()); // Equal objects should have equal hashCodes
    }

    @Test
    public void testToString() {
        ProjectEntity project = new ProjectEntity(1L, "Test Project");
        String expectedString = "ProjectEntity{id=1, name='Test Project'}";
        assertEquals(expectedString, project.toString());
    }
}
