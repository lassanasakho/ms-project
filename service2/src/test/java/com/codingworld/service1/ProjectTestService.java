package com.codingworld.service1;

import com.codingworld.service1.repository.ProjectRepository;
import com.codingworld.service1.service.ProjectService;
import com.codingworld.service1.service.ProjectServiceImpl;
import domain.ProjectEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
public class ProjectTestService     {

    @Mock
    private ProjectRepository projectRepository;

    @InjectMocks
    private ProjectServiceImpl projectService;

    @Test
    void testCreateProject() {
        // Arrange
        ProjectEntity mockProject = new ProjectEntity();
        mockProject.setId(1L);
        mockProject.setName("Test Project");
        when(projectRepository.saveAndFlush(any(ProjectEntity.class))).thenReturn(mockProject);

        // Act
        ProjectEntity createdProject = projectService.createProject(new ProjectEntity());

        // Assert
        assertEquals(mockProject.getId(), createdProject.getId());
        assertEquals(mockProject.getName(), createdProject.getName());
    }
}
