package com.codingworld.service1.controller;

import com.codingworld.service1.service.ProjectService;
import domain.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/rs")
public class Service1Controller {
    @Autowired
        private ProjectService projectService;

  @GetMapping(name = "/project",value = "/project")
  public String sayHello() {

    return "Hello From project Service";
  }
  @PostMapping(name = "/project",value = "/project")
    public void createProject(@Valid @RequestBody ProjectEntity project) {
        projectService.createProject(project);
        // create project
    }

}
