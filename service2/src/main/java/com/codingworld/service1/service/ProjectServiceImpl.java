package com.codingworld.service1.service;

import com.codingworld.service1.repository.ProjectRepository;
import domain.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    private ProjectRepository projectRepository;
    @Override
    public ProjectEntity createProject(ProjectEntity project) {
        return projectRepository.saveAndFlush(project);
    }
}
