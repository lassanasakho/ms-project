package com.codingworld.service1.service;

import domain.ProjectEntity;
import org.springframework.stereotype.Service;

@Service
public interface ProjectService {
    ProjectEntity createProject(ProjectEntity project);
}
